# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

EAPI=7
inherit cmake udev xdg-utils

DESCRIPTION="Hantek DSO6022 USB digital signal oscilloscope"
HOMEPAGE="https://github.com/OpenHantek/OpenHantek6022"
SRC_URI="https://github.com/OpenHantek/OpenHantek6022/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
QTMIN="5.4"

IUSE=""

DEPEND=">=dev-qt/qtgui-${QTMIN}:5
	>=dev-qt/qtwidgets-${QTMIN}:5
	>=dev-qt/qtopengl-${QTMIN}:5
	>=dev-qt/qtprintsupport-${QTMIN}:5
	virtual/opengl
	dev-libs/libusb:1
	sci-libs/fftw:3.0"

RDEPEND="${DEPEND} sci-electronics/electronics-menu"

KEYWORDS="~amd64"

S="${WORKDIR}/OpenHantek6022-"${PV}

src_install() {
	default
	cmake_src_install
	udev_dorules ${S}/utils/udev_rules/60-openhantek.rules
}

pkg_postinst() {
        udev_reload
	xdg_icon_cache_update
}

pkg_postrm() {
        udev_reload
	xdg_icon_cache_update
}
