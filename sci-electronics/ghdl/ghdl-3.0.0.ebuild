# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ADA_COMPAT=( gnat_2021 gcc_12 gcc_13 )
LLVM_MAX_SLOT=15        # Check "configure" script for supported LLVM versions.

inherit ada edo llvm toolchain-funcs

DESCRIPTION="Open-source analyzer, compiler, and simulator for VHDL 2008/93/87"
HOMEPAGE="https://ghdl.github.io/ghdl/
	https://github.com/ghdl/ghdl/"

UVVM_VERSION="2023.03.21"
UVVM_S="${WORKDIR}/UVVM-${UVVM_VERSION}"

OSVVM_VERSION="2022.06"
OSVVM_S="${WORKDIR}/OSVVM-${OSVVM_VERSION}"

SLOT="0"
IUSE="llvm xilinx-vivado uvvm osvvm lattice intel"
LICENSE="GPL-2+
	uvvm? ( Apache-2.0 )
	osvvm? ( Apache-2.0 )"
REQUIRED_USE="${ADA_REQUIRED_USE}"

RDEPEND="
	${ADA_DEPS}
	llvm? ( <sys-devel/llvm-$((${LLVM_MAX_SLOT} + 1)):= )
"
DEPEND="
	${RDEPEND}
"
BDEPEND="
	dev-util/patchelf
"

if [[ ${PV} == *9999* ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/ghdl/${PN}.git"
else
	SRC_URI="https://github.com/ghdl/${PN}/archive/v${PV}.tar.gz
		-> ${P}.tar.gz
		uvvm? ( https://github.com/UVVM/UVVM/archive/v${UVVM_VERSION}.tar.gz
		-> UVVM-${UVVM_VERSION}.tar.gz )
		osvvm? ( https://github.com/OSVVM/OSVVM/archive/${OSVVM_VERSION}.tar.gz
		-> OSVVM-${OSVVM_VERSION}.tar.gz )"
	KEYWORDS="amd64 ~x86"
fi

PATCHES=( "${FILESDIR}"/${PN}-3.0.0-no-pyunit.patch )

pkg_setup() {
	ada_pkg_setup

	use llvm && llvm_pkg_setup
}

src_prepare() {
	default

	sed -i "s|ar rc|$(tc-getAR) rc|g" Makefile.in || die
}

src_configure() {
	tc-export CC CXX

	local -a myconf=(
		--disable-werror

		--libdir=$(get_libdir)
		--prefix=/usr

		--enable-libghdl
		--enable-synth
	)

	if use llvm ; then
		myconf+=( --with-llvm-config=llvm-config )
	fi

	# Not a autotools script!
	edo sh ./configure "${myconf[@]}"
}

src_compile() {
	default

	patchelf --set-soname libghw.so lib/libghw.so || die
}

src_install() {
	default

	if use xilinx-vivado ; then
		edo sh "${D}/usr/lib/ghdl/vendors/compile-xilinx-vivado.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl93 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
		edo sh "${D}/usr/lib/ghdl/vendors/compile-xilinx-vivado.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl2008 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
	fi

	if use uvvm ; then
		edo sh "${D}/usr/lib/ghdl/vendors/compile-uvvm.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"\
			--source "${UVVM_S}"
	fi

	if use osvvm ; then
		edo sh "${D}/usr/lib/ghdl/vendors/compile-osvvm.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl" \
			--source "${OSVVM_S}"
	fi

	if use lattice ; then
		edo sh "${D}/usr/lib/ghdl/vendors/compile-lattice.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl93 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
		edo sh "${D}/usr/lib/ghdl/vendors/compile-lattice.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl2008 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
	fi

	if use intel ; then
		edo sh "${D}/usr/lib/ghdl/vendors/compile-intel.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl93 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
		edo sh "${D}/usr/lib/ghdl/vendors/compile-intel.sh" \
			--all \
			--no-warnings \
			--halt-on-error \
			--vhdl2008 \
			--output "${D}/usr/lib/ghdl/" \
			--ghdl "${D}/usr/bin/ghdl"
	fi
}
