# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit flag-o-matic git-r3 autotools
EGIT_REPO_URI="https://github.com/f14h/prelink.git"

DESCRIPTION="The execstack package contains a utility which modifies (or adds) the GNU_STACK program header to ELF binaries, and allows changing the executable flag."
HOMEPAGE="https://github.com/f14h/prelink"

SLOT="0"
LICENSE="GPL-2"

DOCS=( README )

PATCHES=(
	"${FILESDIR}"/fsync.patch
	"${FILESDIR}"/hurd.patch
	"${FILESDIR}"/init.patch
	"${FILESDIR}"/md5sha.patch
	"${FILESDIR}"/timestamp.patch
)

src_compile() {
	emake -C src execstack || die "emake failed"
}

src_install() {
	dodoc ${DOCS}
	doman doc/execstack.8
	dobin src/execstack
}
