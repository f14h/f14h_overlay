# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{5..12} )

inherit distutils-r1

DESCRIPTION="Sentinelsat makes searching, downloading and retrieving the metadata of Sentinel satellite images from the Copernicus Open Access Hub easy."
HOMEPAGE="https://github.com/sentinelsat/sentinelsat"
SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${P}"

LICENSE="GPL-3.0"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND="dev-python/requests[${PYTHON_USEDEP}]
    dev-python/click[${PYTHON_USEDEP}]
    dev-python/html2text[${PYTHON_USEDEP}]
    dev-python/tqdm[${PYTHON_USEDEP}]
    dev-python/geomet[${PYTHON_USEDEP}]
    >=dev-python/geojson-2.0[${PYTHON_USEDEP}]"
