# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..12} pypy3 )
PYTHON_REQ_USE=""

inherit distutils-r1

DESCRIPTION="Minimal class to download shared files from Google Drive."
HOMEPAGE="https://github.com/ndrplz/google-drive-downloader"
SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=dev-python/requests-2.10[${PYTHON_USEDEP}]
"
