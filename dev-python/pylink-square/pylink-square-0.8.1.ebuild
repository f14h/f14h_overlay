# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6..12} )

inherit distutils-r1

DESCRIPTION="Python interface for SEGGER J-Link"
HOMEPAGE="http://www.github.com/Square/pylink"
SRC_URI="https://github.com/square/pylink/archive/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/pylink-${PV}"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND=">=dev-python/psutil-5.2.2[${PYTHON_USEDEP}]
    dev-python/six[${PYTHON_USEDEP}]
    dev-python/future[${PYTHON_USEDEP}]"

distutils_enable_tests setup.py
distutils_enable_sphinx docs
