# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYPI_NO_NORMALIZE=1
PYPI_PN="AllanTools"
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{7..12} pypy3 )

inherit distutils-r1 pypi

DESCRIPTION="Allan deviation and related time/frequency statistics"
HOMEPAGE="https://github.com/aewallin/allantools
	https://pypi.org/project/AllanTools"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/scipy[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
	dev-python/sphinx-rtd-theme[${PYTHON_USEDEP}]
"

distutils_enable_tests pytest
