# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{7..12} )

inherit distutils-r1

DESCRIPTION="Python bindings and utilities for GeoJSON"
HOMEPAGE="https://github.com/geomet/geomet"
SRC_URI="https://github.com/jazzband/geojson/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/geojson-${PV}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""
