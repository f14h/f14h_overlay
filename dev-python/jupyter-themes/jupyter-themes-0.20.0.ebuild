# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{4..12} )

inherit distutils-r1

DESCRIPTION="Custom Jupyter Notebook Themes"
HOMEPAGE="https://github.com/dunovank/jupyter-themes"
SRC_URI="https://github.com/dunovank/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${P}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=">=dev-python/matplotlib-1.4.3[${PYTHON_USEDEP}]
	dev-python/jupyter-core[${PYTHON_USEDEP}]
	>=dev-python/notebook-5.6.0[${PYTHON_USEDEP}]
	>=dev-python/lesscpy-0.11.2[${PYTHON_USEDEP}]
	>=dev-python/ipython-5.4.1[${PYTHON_USEDEP}]
"
