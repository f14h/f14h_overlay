# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..12} pypy3 )
PYTHON_REQ_USE=""

inherit distutils-r1

DESCRIPTION="Model summary in PyTorch similar to model.summary() in Keras"
HOMEPAGE="https://github.com/sksq96/pytorch-summary"
SRC_URI="mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	dev-python/numpy[${PYTHON_USEDEP}]
	sci-libs/pytorch[${PYTHON_USEDEP}]
"
