# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6..12} )

inherit distutils-r1

DESCRIPTION="TensorBoardX lets you watch Tensors Flow without Tensorflow"
HOMEPAGE="https://github.com/lanpa/tensorboardX"
SRC_URI="https://github.com/lanpa/tensorboardX/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/tensorboardX-${PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"

DOCS=( HISTORY.rst README.md )

distutils_enable_tests pytest

RDEPEND="dev-python/numpy[${PYTHON_USEDEP}]
    >=dev-python/protobuf-python-3.8.0[${PYTHON_USEDEP}]"

BDEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
         test? (
                dev-python/future[${PYTHON_USEDEP}]
                dev-python/matplotlib[${PYTHON_USEDEP}]
                dev-libs/crc32c
         )"



src_prepare() {
    rm -fv compile.sh
    sed -i '/compileProtoBuf()$/d' setup.py
    sed -i '/pip install/d' setup.py

    distutils-r1_python_prepare_all
}

src_install() {
    rm -fv tensorboardX/proto/*pb2*.py
    protoc tensorboardX/proto/*.proto --python_out=.

    distutils-r1_src_install
}
