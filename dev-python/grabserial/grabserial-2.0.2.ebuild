# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6..12} )

inherit distutils-r1

DESCRIPTION="Grabserial - python-based serial dump and timing program - good for embedded Linux development"
HOMEPAGE="https://elinux.org/Grabserial"
SRC_URI="https://github.com/tbird20d/grabserial/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${PV}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND=">=dev-python/pyserial-2.6[${PYTHON_USEDEP}]
"
