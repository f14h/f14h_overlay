# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..12} )

inherit distutils-r1

DESCRIPTION="GeoMet - Convert GeoJSON to WKT/WKB, and vice versa"
HOMEPAGE="https://github.com/geomet/geomet"
SRC_URI="https://github.com/geomet/geomet/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/geomet-${PV}"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-python/six[${PYTHON_USEDEP}]
    dev-python/click[${PYTHON_USEDEP}]"
