# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

# TODO: Transform automagically
MY_PN="hpc_helpers"
BRANCH="restructure"

DESCRIPTION="convenient macros & functions for CUDA"
HOMEPAGE="https://gitlab.rlp.net/pararch/hpc_helpers"
SRC_URI="https://gitlab.rlp.net/pararch/${MY_PN}/-/archive/${BRANCH}/${MY_PN}-${BRANCH}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${MY_PN}-${BRANCH}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

BDEPEND=""
DEPEND=">=dev-util/nvidia-cuda-toolkit-9.0:0="

RDEPEND="${DEPEND}"

src_install()  {
	dodoc README.md

	doheader -r include/helpers
}
