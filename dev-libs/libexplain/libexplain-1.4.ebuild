# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

AUTOTOOLS_AUTORECONF=yes
AUTOTOOLS_IN_SOURCE_BUILD=1

inherit autotools

DESCRIPTION="Library which may be used to explain Unix and Linux system call errors"
HOMEPAGE="http://libexplain.sourceforge.net/"
SRC_URI="http://libexplain.sourceforge.net/${P}.tar.gz"

SLOT="0"
KEYWORDS="amd64 ppc ~ppc64 x86 ~amd64-linux ~x86-linux"
LICENSE="GPL-3 LGPL-3"
IUSE="static-libs"

RDEPEND="
	sys-libs/libcap
	>=sys-libs/glibc-2.11
	sys-process/lsof"
DEPEND="${RDEPEND}
	>=sys-kernel/linux-headers-2.6.35
	app-text/ghostscript-gpl
	sys-apps/groff
"
# Test fails with:
# This is not a bug, but it does indicate where libexplain's ioctl support
# could be improved.
RESTRICT="test"

DOCS=( README )

PATCHES=(
	"${FILESDIR}"/02_alpha-fcntl-h.patch
	"${FILESDIR}"/03_fsflags-4.5.patch
	"${FILESDIR}"/04_test-t0274a.patch
	"${FILESDIR}"/05_largefile.patch
	"${FILESDIR}"/06_sysctl.patch
	"${FILESDIR}"/07_ustat.patch
	"${FILESDIR}"/08_hppa.patch
	"${FILESDIR}"/fix-tests-sed.patch
	"${FILESDIR}"/gcc-10.patch
	"${FILESDIR}"/linux5.11.patch
	"${FILESDIR}"/nettstamp-needs-types.patch
	"${FILESDIR}"/sanitize-bison.patch
	"${FILESDIR}"/termiox-no-more-exists-since-kernel-5.12.patch
	"${FILESDIR}"/typos.patch
	"${FILESDIR}"/use-command-v.patch
)

src_prepare() {
	# Portage incompatible test
	sed \
		-e '/t0524a/d' \
		-e '/t0363a/d' \
		-i Makefile.in || die

	cp "${S}"/etc/configure.ac "${S}" || die

	default
	eautoreconf
}
