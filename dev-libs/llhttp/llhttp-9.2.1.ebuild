# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Port of http_parser to llparse"
HOMEPAGE="https://llhttp.org"

if [[ ${PV} == *9999 ]] ; then
	EGIT_REPO_URI="https://github.com/nodejs/llhttp"
	EGIT_SUBMODULES=()
	inherit git-r3
else
	SRC_URI="https://github.com/nodejs/llhttp/archive/refs/tags/release/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64"
	S="${WORKDIR}/${PN}-release-v${PV}"
fi

LICENSE="MIT"
SLOT="0"
IUSE="static"

DEPEND="
"

BDEPEND="
"

PATCHES=(
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_STATIC_LIBS=$(usex static on off)
	)
	cmake_src_configure
}
