# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION=" Cross-platform, efficient, customizable, and robust asynchronous HTTP(S)/WebSocket server C++ library with the right balance between performance and ease of use"
HOMEPAGE="https://stiffstream.com/en/products/restinio.html"

if [[ ${PV} == *9999 ]] ; then
	EGIT_REPO_URI="https://github.com/Stiffstream/restinio"
	EGIT_SUBMODULES=()
	inherit git-r3
else
	SRC_URI="https://github.com/Stiffstream/restinio/releases/download/v.${PV}/${P}-full.tar.bz2 -> ${P}.tar.bz2"
	KEYWORDS="amd64"
	S="${WORKDIR}/${P}/dev"
fi

LICENSE="BSD"
SLOT="0"
IUSE="benchmark examples test +boost profiler"
RESTRICT="!test? ( test )"

DEPEND="
	!boost? ( dev-cpp/asio )
	boost? ( dev-libs/boost )
	sys-libs/zlib
	dev-libs/openssl
	dev-libs/libpcre
	dev-libs/libpcre2
	dev-ml/fmt
	dev-cpp/expected-lite
	dev-libs/llhttp
	test? ( dev-cpp/catch )
"

BDEPEND="
"

PATCHES=(
	"${FILESDIR}/target.patch"
)

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DRESTINIO_TEST=$(usex test on off)
		-DRESTINIO_SAMPLE=$(usex examples on off)
		-DRESTINIO_BENCHMARK=$(usex benchmark on off)
		-DRESTINIO_GCC_CODE_COVERAGE=$(usex profiler on off)
		-DRESTINIO_CLANG_TIDY=off
		-DRESTINIO_WITH_SOBJECTIZER=on
		-DRESTINIO_DEP_FMT=system
		-DRESTINIO_DEP_STANDALONE_ASIO=system
		-DRESTINIO_DEP_BOOST_ASIO=system
		-DRESTINIO_ASIO_SOURCE=$(usex boost boost standalone)
		-DRESTINIO_DEP_EXPECTED_LITE=system
		-DRESTINIO_DEP_CATCH2=system
		-DRESTINIO_DEP_SOBJECTIZER=local
		-DRESTINIO_DEP_LLHTTP=system
	)
	cmake_src_configure
}
