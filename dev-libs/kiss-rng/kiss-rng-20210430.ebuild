# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

MY_PN="kiss_rng"
COMMIT="24a9848ca3fbfb83567064f305647041072eaacd"

DESCRIPTION="Fast random number generator for C++ and CUDA"
HOMEPAGE="https://github.com/sleeepyjack/kiss_rng"
SRC_URI="https://github.com/sleeepyjack/${MY_PN}/archive/${COMMIT}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${MY_PN}-${COMMIT}"

LICENSE="all-rights-reserved"
RESTRICT="bindist mirror"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
PATCHES="
	${FILESDIR}/${PN}-remove-cpm.patch
"

BDEPEND=""

DEPEND="
	dev-libs/hpc-helpers
	>=dev-util/nvidia-cuda-toolkit-9.0:0=
"

RDEPEND="${DEPEND}"

src_prepare() {
	rm -f ${WORKDIR}/cmake/CPM.cmake
	eapply_user
	cmake_src_prepare
}

src_install()  {
        doheader -r include/kiss
}
