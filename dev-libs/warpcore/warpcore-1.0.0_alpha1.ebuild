# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

# TODO: Transform automagically
MY_PV="1.0.0-alpha.1"

DESCRIPTION="A Library for fast Hash Tables on GPUs"
HOMEPAGE="https://github.com/sleeepyjack/warpcore"
SRC_URI="https://github.com/sleeepyjack/${PN}/archive/refs/tags/${MY_PV}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${MY_PV}"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
PATCHES="
	${FILESDIR}/${PN}-remove-cpm.patch
"

BDEPEND=""

DEPEND="
	dev-libs/hpc-helpers
	dev-libs/kiss-rng
	>=dev-util/nvidia-cuda-toolkit-11.2:0=
"

RDEPEND="${DEPEND}"

src_prepare() {
	rm -f ${WORKDIR}/cmake/CPM.cmake
	eapply_user
        cmake_src_prepare
}

src_install()  {
        dodoc README.md

        doheader -r include/warpcore
}
