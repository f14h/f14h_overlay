# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8

inherit autotools

DESCRIPTION="A simple, fast library to read and write CSV data"
HOMEPAGE="http://sourceforge.net/projects/libcsv/"
SRC_URI="mirror://sourceforge/libcsv/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc examples static-libs"
