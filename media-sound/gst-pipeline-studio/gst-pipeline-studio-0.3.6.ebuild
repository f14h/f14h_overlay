# Copyright 2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	addr2line@0.24.1
	adler2@2.0.0
	android-tzdata@0.1.1
	android_system_properties@0.1.5
	ansi_term@0.12.1
	anyhow@1.0.87
	async-channel@2.3.1
	atomic_refcell@0.1.13
	atty@0.2.14
	autocfg@1.3.0
	backtrace@0.3.74
	base64@0.9.3
	bitflags@0.9.1
	bitflags@1.3.2
	bitflags@2.6.0
	bumpalo@3.16.0
	byteorder@1.5.0
	cairo-rs@0.20.1
	cairo-sys-rs@0.20.0
	cc@1.1.18
	cfg-expr@0.16.0
	cfg-if@1.0.0
	chrono@0.4.38
	clap@2.34.0
	concurrent-queue@2.5.0
	core-foundation-sys@0.8.7
	crossbeam-utils@0.8.20
	dtoa@0.4.8
	either@1.13.0
	equivalent@1.0.1
	error-chain@0.10.0
	event-listener-strategy@0.5.2
	event-listener@5.3.1
	failure@0.1.8
	failure_derive@0.1.8
	field-offset@0.3.6
	futures-channel@0.3.30
	futures-core@0.3.30
	futures-executor@0.3.30
	futures-io@0.3.30
	futures-macro@0.3.30
	futures-task@0.3.30
	futures-util@0.3.30
	gdk-pixbuf-sys@0.20.1
	gdk-pixbuf@0.20.1
	gdk4-sys@0.9.0
	gdk4-win32-sys@0.9.0
	gdk4-win32@0.9.0
	gdk4@0.9.0
	gimli@0.31.0
	gio-sys@0.20.1
	gio@0.20.1
	glib-macros@0.20.3
	glib-sys@0.20.2
	glib@0.20.3
	gobject-sys@0.20.1
	graphene-rs@0.20.1
	graphene-sys@0.20.1
	gsk4-sys@0.9.0
	gsk4@0.9.0
	gst-plugin-gtk4@0.13.1
	gst-plugin-version-helper@0.8.2
	gstreamer-base-sys@0.23.0
	gstreamer-base@0.23.1
	gstreamer-gl-sys@0.23.0
	gstreamer-gl@0.23.0
	gstreamer-sys@0.23.0
	gstreamer-video-sys@0.23.0
	gstreamer-video@0.23.0
	gstreamer@0.23.1
	gtk4-macros@0.9.1
	gtk4-sys@0.9.0
	gtk4@0.9.1
	hashbrown@0.14.5
	heck@0.3.3
	heck@0.5.0
	hermit-abi@0.1.19
	iana-time-zone-haiku@0.1.2
	iana-time-zone@0.1.60
	idna@0.1.5
	indexmap@2.5.0
	itertools@0.13.0
	itoa@0.4.8
	itoa@1.0.11
	js-sys@0.3.70
	lazy_static@1.5.0
	libc@0.2.158
	linked-hash-map@0.5.6
	log@0.3.9
	log@0.4.22
	matches@0.1.10
	memchr@2.7.4
	memoffset@0.9.1
	miniz_oxide@0.8.0
	muldiv@1.0.1
	num-integer@0.1.46
	num-rational@0.4.2
	num-traits@0.2.19
	object@0.36.4
	once_cell@1.19.0
	option-operations@0.5.0
	pango-sys@0.20.1
	pango@0.20.1
	parking@2.2.1
	paste@1.0.15
	percent-encoding@1.0.1
	pin-project-lite@0.2.14
	pin-utils@0.1.0
	pkg-config@0.3.30
	proc-macro-crate@3.2.0
	proc-macro-error-attr@1.0.4
	proc-macro-error@1.0.4
	proc-macro2@1.0.86
	quote@1.0.37
	ron@0.3.0
	rustc-demangle@0.1.24
	rustc_version@0.4.1
	ryu@1.0.18
	safemem@0.3.3
	semver@1.0.23
	serde-xml-any@0.0.3
	serde@1.0.210
	serde_any@0.5.0
	serde_derive@1.0.210
	serde_json@1.0.128
	serde_spanned@0.6.7
	serde_urlencoded@0.5.5
	serde_yaml@0.7.5
	shlex@1.3.0
	simplelog@0.11.2
	slab@0.4.9
	smallvec@1.13.2
	strsim@0.8.0
	structopt-derive@0.4.18
	structopt@0.3.26
	syn@1.0.109
	syn@2.0.77
	synstructure@0.12.6
	system-deps@7.0.2
	target-lexicon@0.12.16
	termcolor@1.1.3
	textwrap@0.11.0
	thiserror-impl@1.0.63
	thiserror@1.0.63
	tinyvec@1.8.0
	tinyvec_macros@0.1.1
	toml@0.4.10
	toml@0.8.19
	toml_datetime@0.6.8
	toml_edit@0.22.20
	unicode-bidi@0.3.15
	unicode-ident@1.0.12
	unicode-normalization@0.1.23
	unicode-segmentation@1.11.0
	unicode-width@0.1.13
	unicode-xid@0.2.5
	url@1.7.2
	vec_map@0.8.2
	version-compare@0.2.0
	version_check@0.9.5
	wasm-bindgen-backend@0.2.93
	wasm-bindgen-macro-support@0.2.93
	wasm-bindgen-macro@0.2.93
	wasm-bindgen-shared@0.2.93
	wasm-bindgen@0.2.93
	winapi-i686-pc-windows-gnu@0.4.0
	winapi-util@0.1.9
	winapi-x86_64-pc-windows-gnu@0.4.0
	winapi@0.3.9
	windows-core@0.52.0
	windows-sys@0.52.0
	windows-sys@0.59.0
	windows-targets@0.52.6
	windows_aarch64_gnullvm@0.52.6
	windows_aarch64_msvc@0.52.6
	windows_i686_gnu@0.52.6
	windows_i686_gnullvm@0.52.6
	windows_i686_msvc@0.52.6
	windows_x86_64_gnu@0.52.6
	windows_x86_64_gnullvm@0.52.6
	windows_x86_64_msvc@0.52.6
	winnow@0.6.18
	xml-rs@0.6.1
	xml-rs@0.8.22
	yaml-rust@0.4.5
"

inherit meson cargo desktop

DESCRIPTION="GstPipelineStudio aims to provide a graphical user interface to the GStreamer framework."
HOMEPAGE="https://gitlab.freedesktop.org/dabrain34/GstPipelineStudio"
SRC_URI="
	https://gitlab.freedesktop.org/dabrain34/GstPipelineStudio/-/archive/${PV}/GstPipelineStudio-${PV}.tar.gz
	${CARGO_CRATE_URIS}
"

LICENSE="GPL-3+"
# Dependent crate licenses
LICENSE+="
	Apache-2.0-with-LLVM-exceptions MIT MPL-2.0 Unicode-DFS-2016
	|| ( Apache-2.0 Boost-1.0 )
"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=gui-libs/gtk-4.0.0:4
	>=dev-libs/glib-2.66.0:2
	>=media-libs/gstreamer-1.20:1.0
	>=media-libs/gst-plugins-base-1.20:1.0
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/GstPipelineStudio-${PV}"

EMESON_BUILDTYPE="release"

src_configure() {
	local emesonargs=(
	)
	meson_src_configure
}

src_install() {
	cargo_src_install

	doicon data/icons/org.freedesktop.dabrain34.GstPipelineStudio.svg
	domenu ${BUILD_DIR}/data/org.freedesktop.dabrain34.GstPipelineStudio.desktop
}
